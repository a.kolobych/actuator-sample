## Launch cluster proxy

**Deploying time error**
>The connection to the server satisfyer-bot.xyz:8001 was refused - did you specify the right host or port?

**It requires launch proxy on master node to make API accessible**
>```sudo kubectl proxy --address='0.0.0.0' --disable-filter=true```


## kubectl cheatsheet
`kubectl scale deployment DEPLOYMENT_NAME --replicas 3` -   scale deployment up to 3 replicas
`kubectl port-forward --address 0.0.0.0 actuator-sample-54d54b9dd7-xngwz 8888:8080` - port forwarding outside